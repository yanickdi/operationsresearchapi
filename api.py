﻿import numpy as np

class TransportationSimplex:
    """ This class implements the Transportation Simplex"""

    def __init__(self, costs_matrix, basic_vars):
        self.costs_matrix = costs_matrix
        self.basic_vars = basic_vars

    def solve(self):
        """Solves the problem using the algorithm on p. 198"""
        #Step 1
        self.__calculateDualVariables()
        pass

    def __calculateDualVariables(self):
        """Returns two lists of dual variables. The first list is the u_i list, the second the v_j list"""
        #c_ij = u_i + v_j
        u_list = [None] * self.m   #vertical list
        v_list = [None] * self.n   #horizontal list
        #set the first dual to zero
        u_list[0] = 0
        #for pos, in enumerate(u_list, start=1)

    @property
    def n(self):
        """Getter: The number of columns in the problem"""
        return len(self.costs_matrix[0])

    @property
    def m(self):
        """Getter: The number of rows in the problem"""
        return len(self.costs_matrix)

if __name__ == '__main__':
    m = np.array([[12, 3, 6, 5],[0, 10, 4, 3],[5, 6, 1, 7],[0, 2, 4, 8]])
    hung = Hungarian(m)
    hung.minimize()

    #print(m[0,::].reshape(4).argmin())
    #print(m.argmin(axis=0)) #vertical
    #print(m.argmin(axis=1))
    