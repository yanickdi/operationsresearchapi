﻿import numpy as np
import munkres

class Hungarian:
    """This class just uses the Munkres implementation for Python"""
    
    def __init__(self, matrix):
        self.origin_matrix = matrix
        self.matrix = np.copy(matrix)

    def minimize(self):
        """
            Minizes at first all rows, then all columns f the matrix. At the end every row and every
            column has at least one zero in it
        """
        self.__minimize_rows()
        self.matrix = self.matrix.T
        self.__minimize_rows()
        self.matrix = self.matrix.T

    def __minimize_rows(self):
        """minimizes all rows of the matrix, such that every row contains at least one zero"""
        m = self.matrix
        for r in range(m.shape[0]):
            min_col = m[r,].argmin()
            min_value = m[r, min_col]
            for c in range(m.shape[1]):
                m[r,c] = m[r,c] - min_value

    def markRows(self):
        """
            Tries to mark as few rows/columns as possible. Returns the number of rows/cols marked.
            A solution can be found if number is equal to matrix shape width (eg. 4x4 matrix must be 4)
        """
        m = self.matrix
        #find row with only one zero
        for r in range(m.shape[0]):
            zero_list = self.__position_of_zeros(m[r,:])
            if len(zero_list) == 1:
                position_of_zero = zero_list[0]


    def __position_of_zeros(self, line):
        """Returns a list of indices, each giving the position of a zero in a given matrix `line`"""
        zero_list = []
        for i in range(line.shape[0]):
            if line[i] == 0:
                zero_list.append(i)
        return zero_list

if __name__ == '__main__':
    m = np.array([
        [ 0,  5,  8,  9,  4,  9, 10],
        [ 5,  0, 13, 11,  9, 11, 12],
        [ 8, 13,  0,  9,  4,  9,  5],
        [ 9, 11,  9,  0,  5,  9, 11],
        [ 4,  9,  4,  5,  0,  5,  6],
        [ 9, 11,  9,  9,  5,  0,  8],
        [10, 12,  5, 11,  6,  8,  0]])
    hung = Hungarian(m)

    if False:
        m = np.array([[12, 3, 6, 5],[0, 10, 4, 3],[5, 6, 1, 7],[0, 2, 4, 8]])
        hung = Hungarian(m)
        hung.minimize()
        print(hung.matrix)
        hung.markRows()
        #print(m[0,::].reshape(4).argmin())
        #print(m.argmin(axis=0)) #vertical
        #print(m.argmin(axis=1))
    