import numpy as np

class TSP_NearestNeighbor:
    def __init__(self, cost_matrix):
        self.m = cost_matrix
        self.len = m.shape[0]
        assert self.len >= 2

    def solve(self, startPoint = 0):
        """Returns two values: a list of optimal indices and the costs"""
        l = [startPoint]
        excluded = [startPoint]
        costs = 0

        while len(excluded) < self.len:
            actual_position = l[-1]
            distance_list_from_actual = m[actual_position, :]
            nearest_index = self._find_nearest(distance_list_from_actual, excluded)
            excluded.append(nearest_index)
            l.append(nearest_index)
            costs += distance_list_from_actual[nearest_index]

        #at the end, append the startPoint
        costs += m[l[-1], startPoint]
        l.append(startPoint)
        return l, costs

    def _find_nearest(self, candidates, excluded):
        """
            finds the nearest candidate in a given `candidates` list. ignores indices included in `excluded`
            returns the nearest candidate's index.
        """
        nearest_ind = None
        for i, candidate in enumerate(candidates):
            if i in excluded:
                continue
            if nearest_ind is None or candidates[nearest_ind] > candidate:
                nearest_ind = i
        return nearest_ind


if __name__ == '__main__':
    m = np.array([
        [ 0,  5,  8,  9,  4,  9, 10],
        [ 5,  0, 13, 11,  9, 11, 12],
        [ 8, 13,  0,  9,  4,  9,  5],
        [ 9, 11,  9,  0,  5,  9, 11],
        [ 4,  9,  4,  5,  0,  5,  6],
        [ 9, 11,  9,  9,  5,  0,  8],
        [10, 12,  5, 11,  6,  8,  0]])

    tsp = TSP_NearestNeighbor(m)
    for i in range(tsp.len):
        route, costs = tsp.solve(startPoint=i)
        route_string = '-'.join(str(r) for r in route)
        print('Start: {}, Route: {}, Costs: {}'.format(i, route_string, costs))