import numpy as np
import math
import sys

class VRP_ClarkeAndWrightSavings:
    def __init__(self, cost_matrix, route_duration_constraint = None, demand_list = None, truck_capacity = None):
        """
            cost_matrix: a numpy array, (0,0) has to be the depot itself
        """
        self.m = cost_matrix
        self.len = cost_matrix.shape[0]
        self.route_duration_constraint = route_duration_constraint
        if truck_capacity:
            if not demand_list:
                raise ValueError('demand_list must be given if truck_capacity contraint is given!')
        if demand_list and len(demand_list) != self.len-1:
            raise ValueError('demand list has to be the same length as cost_matrix width -1')
        self.truck_capacity = truck_capacity
        self.demand_list = demand_list
        self.savings_matrix = None

    def solve(self):
        """
            solves the problem
        
            returns a dictionary
        """
        self._calc_savings()
        routes = [[0,i,0] for i in range(1, self.len)]
        for iteration, saving in enumerate(self.savings_list):
            #try to merge
            route_from = self.find_route_of_customer(routes, saving['from'])
            route_to = self.find_route_of_customer(routes, saving['to'])
            #both first or last?
            first_or_last = route_from[-2] == saving['from'] and route_to[1] == saving['to']
            if first_or_last and route_from != route_to:
                new_route = route_from[0:-1] + route_to[1:]
                costs = self.route_costs(new_route)
                route_is_feasible = True
                if self.route_duration_constraint:
                   route_is_feasible = route_is_feasible and costs <= self.route_duration_constraint
                if self.truck_capacity:
                    truck_size = self.route_truck_size(new_route)
                    route_is_feasible = route_is_feasible and truck_size <= self.truck_capacity
                if route_is_feasible:
                    #now we would have to check the truck_capacity constraint - but demand is not implemented yet
                    #we can join the routes:
                    routes.remove(route_from)
                    routes.remove(route_to)
                    routes.append(new_route)
        costs_list = [self.route_costs(route) for route in routes]
        return {'routes' : routes, 'costs': costs_list, 'total_costs' : sum(costs_list)}

    def find_route_of_customer(self, routes, customer):
        """finds the route of the given customer"""
        for route in routes:
            if customer in route:
                return route

    def print_routes(self, routes):
        total_costs = 0
        for route in routes:
            c = self.route_costs(route)
            print('Route: {}'.format('-'.join(str(r) for r in route)))
            print('   cost: {:.2f}'.format(c))
            if self.truck_capacity:
                size = self.route_truck_size(route)
                print('   size: {:.0f}'.format(size))
            total_costs += c
        print('-----')
        print('Total costs: {:.2f}'.format(total_costs))

    def route_truck_size(self, route):
        """Returns the theoretical size of the truck that serves the route"""
        size = 0
        for customer in route:
            if customer != 0:
                size += self.demand_list[customer-1]  #demand list is shorter than matrix because of the depot
        return size

    def route_costs(self, route):
        """returns the costs of a given `route`"""
        costs = 0
        for i in range(1, len(route)):
            prev = route[i-1]
            act = route[i]
            costs += self.m[prev, act]
        return costs



    def _calc_savings(self):
        """
            creates a savings matrix and saves it inside the obj
            savings:   s[i, j] = d[i, 0] + d[0, j] - d[i, j]

        """
        s = np.zeros(shape=(self.len, self.len))
        self.savings_list = []
        m = self.m
        for i in range(self.len):
            for j in range(self.len):
                #diagonal doesn't make any sense - write None into it
                if i == j or i == 0 or j == 0:
                    s[i,j] = None
                else:
                    s[i,j] = round(m[i, 0] + m[0, j] - m[i, j], 1)
                    self.savings_list.append({
                        'savings' : m[i, 0] + m[0, j] - m[i, j],
                        'from' : i,
                        'to': j})
        #sort savings list descending:
        self.savings_list.sort(key = lambda elem: elem['savings'], reverse = True)
        self.savings_matrix = s

    def print_savings(self):
        """Prints the savings matrix of the given costs matrix"""
        if not self.savings_matrix:
            self._calc_savings()
        print(self.savings_matrix[1:,1:])

    @staticmethod
    def createCostsMatrixFromXYList(xy_list):
        """
            A static method that returns a numpy array from a xy list like [(x1,y1), (x2, y2), (x3,y4), (xn,yn)]
            where x1,y1 have to be the coordinates of the depot and (x2,y2) ... (xn,yn) the customers' coordinates.
            the costs represent the euclidean distances between each
        """
        m = np.zeros(shape=(len(xy_list), len(xy_list)))
        for i in range(len(xy_list)):
            p_origin = xy_list[i]
            for j in range(len(xy_list)):
                p_other = xy_list[j]
                m[i,j] = round(math.sqrt( (p_other[0] - p_origin[0])**2  + (p_other[1] - p_origin[1])**2 ), 1)
        return m

if __name__ == '__main__':
    #example one  (skript):
    xy_list = [(0,0), (4,1), (1,2), (0, 5), (-3, 3), (-2, 1), (-5, 1), (-5, -1), (-1, -3), (3, -2), (6, -1)]
    matrix = VRP_ClarkeAndWrightSavings.createCostsMatrixFromXYList(xy_list)
    vrp = VRP_ClarkeAndWrightSavings(matrix, route_duration_constraint = 16)
    vrp.print_savings()
    solution = vrp.solve()
    vrp.print_routes(solution['routes'])


    #example two (ueb_zettel):
    print('\n\n\n\n')
    m = np.array([
        [ 0,  5,  8,  9,  4,  9, 10],
        [ 5,  0, 13, 11,  9, 11, 12],
        [ 8, 13,  0,  9,  4,  9,  5],
        [ 9, 11,  9,  0,  5,  9, 11],
        [ 4,  9,  4,  5,  0,  5,  6],
        [ 9, 11,  9,  9,  5,  0,  8],
        [10, 12,  5, 11,  6,  8,  0]])
    demand_list = [2, 4, 5, 3, 3, 1]
    vrp = VRP_ClarkeAndWrightSavings(m, route_duration_constraint = 50,
                                        truck_capacity = 6, demand_list = demand_list)
    vrp.print_routes( vrp.solve()['routes'] )